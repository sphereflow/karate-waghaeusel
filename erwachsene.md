Als eine der japanischen Kampfkünste erlaubt es das Karate bis ins hohe Alter betrieben zu werden.  
Hierbei kann jeder die Trainingsintensität nach seinem eigenen Können  
und den körperlichen Gegebenheiten festlegen.  
Somit bietet sich die Möglichkeit den eigenen Fortschritt selbst zu gestalten.  
Es ist zu beachten, dass Karate ein ganzheitliches Bewegungssystem ist.  
Es werden also keine Muskelgruppen in Isolation trainiert und auch  
andere Aspekte wie Koordination und Timing sind von Bedeutung.  
Da der Ursprung des Karate neben Okinawa auch in China liegt,  
wo Kung Fu in den chinesischen Klöstern ausgeübt wurde,  
wird im Karate sowohl Körper, als auch Geist geschult.  

Bei Interesse schicken sie uns eine e-Mail an : info@karate-waghäusel.de
