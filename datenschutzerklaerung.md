# Datenschutzerklärung #

## Grundlegendes ##

Diese Datenschutzerklärung soll die Nutzer dieser Website über die Art,  
den Umfang und den Zweck der Erhebung und Verwendung personenbezogener Daten  
durch den Websitebetreiber : Johann Prescher (e-Mail: Johann.Prescher@karate-waghäusel.de) informieren.  

Der Websitebetreiber nimmt Ihren Datenschutz sehr ernst und behandelt Ihre  
personenbezogenen Daten vertraulich und entsprechend der gesetzlichen Vorschriften.  
Da durch neue Technologien und die ständige Weiterentwicklung dieser Webseite  
Änderungen an dieser Datenschutzerklärung vorgenommen werden können,  
empfehlen wir Ihnen sich die Datenschutzerklärung in regelmäßigen Abständen wieder durchzulesen.  

Definitionen der verwendeten Begriffe (z.B. “personenbezogene Daten” oder “Verarbeitung”)  
finden Sie in Art. 4 DSGVO.  

## Zugriffsdaten ##

Wir, der Websitebetreiber bzw. Seitenprovider, erheben aufgrund unseres berechtigten Interesses  
(s. Art. 6 Abs. 1 lit. f. DSGVO) Daten über Zugriffe auf die Website  
und speichern diese als „Server-Logfiles“ auf dem Server der Website ab.  
Folgende Daten werden so protokolliert:  

• Besuchte Website  
• Uhrzeit zum Zeitpunkt des Zugriffes  
• Menge der gesendeten Daten in Byte  
• Quelle/Verweis, von welchem Sie auf die Seite gelangten  
• Verwendeter Browser  
• Verwendetes Betriebssystem  
• Verwendete IP-Adresse  
• Hostname des zugreifenden Rechners  

Die erhobenen Daten dienen lediglich zur statistischen Auswertung und zur Verbesserung der Website.  
Der Websitebetreiber behält sich allerdings vor, die Server-Logfiles nachträglich zu überprüfen,  
sollten konkrete Anhaltspunkte auf eine rechtswidrige Nutzung hinweisen.  

## Privacy Policy des Hosting Providers ##

Informationen zum Datenschutz unseres Hosting Providers finden sie unter : <https://about.gitlab.com/privacy/>

## Reichweitenmessung & Cookies ##

Diese Website verwendet Cookies zur pseudonymisierten Reichweitenmessung,  
die entweder von unserem Server oder dem Server Dritter an den Browser des Nutzers übertragen werden.  
Bei Cookies handelt es sich um kleine Dateien, welche auf Ihrem Endgerät gespeichert werden.  
Ihr Browser greift auf diese Dateien zu. Durch den Einsatz von Cookies erhöht sich die Benutzerfreundlichkeit  
und Sicherheit dieser Website.  
Falls Sie nicht möchten, dass Cookies zur Reichweitenmessung auf Ihrem Endgerät gespeichert werden,  
können Sie dem Einsatz dieser Dateien hier widersprechen:

• Cookie-Deaktivierungsseite der Netzwerkwerbeinitiative: <http://optout.networkadvertising.org/?c=1#!/>  
• Cookie-Deaktivierungsseite der US-amerikanischen Website: <http://optout.aboutads.info/?c=2#!/>  
• Cookie-Deaktivierungsseite der europäischen Website: <http://optout.networkadvertising.org/?c=1#!/>  

Gängige Browser bieten die Einstellungsoption, Cookies nicht zuzulassen.  
Hinweis: Es ist nicht gewährleistet, dass Sie auf alle Funktionen dieser Website ohne Einschränkungen zugreifen können,  
wenn Sie entsprechende Einstellungen vornehmen.  

## Erfassung und Verarbeitung personenbezogener Daten ##

Der Websitebetreiber erhebt, nutzt und gibt Ihre personenbezogenen Daten nur dann weiter,  
wenn dies im gesetzlichen Rahmen erlaubt ist oder Sie in die Datenerhebung einwilligen.  
Als personenbezogene Daten gelten sämtliche Informationen, welche dazu dienen,  
Ihre Person zu bestimmen und welche zu Ihnen zurückverfolgt werden können –  
also beispielsweise Ihr Name, Ihre E-Mail-Adresse und Telefonnummer.  

Diese Website können Sie auch besuchen, ohne Angaben zu Ihrer Person zu machen.  
Zur Verbesserung unseres Online-Angebotes speichern wir jedoch (ohne Personenbezug)  
Ihre Zugriffsdaten auf diese Website. Zu diesen Zugriffsdaten gehören z. B.  
die von Ihnen angeforderte Datei oder der Name Ihres Internet-Providers.  
Durch die Anonymisierung der Daten sind Rückschlüsse auf Ihre Person nicht möglich.  

Hinweis: An dieser Stelle sollten Sie zusätzlich angeben, welche, wie und warum Sie  
als Webseitenbetreiber personenbezogene Daten verarbeiten.  

Beispiele:  

    • Welche? - Wir verarbeiten personenbezogene Daten wie Vorname, Nachname,  
      IP-Adresse, E-Mail-Adresse, Wohnort, Postleitzahl und Inhaltsangaben aus dem Kontaktformular.  

    • Wie? - Wir verarbeiten personenbezogene Daten nur nach ausdrücklicher  
      Erlaubnis der betreffenden Nutzer und unter Einhaltung der geltenden Datenschutzbestimmungen.  

    • Warum? - Die Verarbeitung der personenbezogenen Daten erfolgt aufgrund unseres berechtigten  
      Interesses zur Erfüllung unserer vertraglich vereinbarten Leistungen und zur  
      Optimierung unseres Online-Angebotes.  

## Umgang mit Kontaktdaten ##

Nehmen Sie mit uns als Websitebetreiber durch die angebotenen Kontaktmöglichkeiten Verbindung auf,  
werden Ihre Angaben gespeichert, damit auf diese zur Bearbeitung und Beantwortung Ihrer Anfrage  
zurückgegriffen werden kann. Ohne Ihre Einwilligung werden diese Daten nicht an Dritte weitergegeben.  

## Umgang mit Kommentaren und Beiträgen ##

Hinterlassen Sie auf dieser Website einen Beitrag oder Kommentar, wird Ihre IP-Adresse gespeichert.  
Dies erfolgt aufgrund unserer berechtigten Interessen im Sinne des Art. 6 Abs. 1 lit. f. DSGVO  
und dient der Sicherheit von uns als Websitebetreiber: Denn sollte Ihr Kommentar gegen geltendes  
Recht verstoßen, können wir dafür belangt werden, weshalb wir ein Interesse an der Identität des  
Kommentar- bzw. Beitragsautors haben.  

## Nutzung von Social-Media-Plugins von Facebook ##

Aufgrund unseres berechtigten Interesses an der Analyse, Optimierung und dem Betrieb unseres  
Online-Angebotes (im Sinne des Art. 6 Abs. 1 lit. f. DSGVO), verwendet diese Website das  
Facebook-Social-Plugin, welches von der Facebook Inc. (1 Hacker Way, Menlo Park, California 94025, USA)  
betrieben wird. Erkennbar sind die Einbindungen an dem Facebook-Logo bzw. an den Begriffen  
„Like“, „Gefällt mir“, „Teilen“ in den Farben Facebooks (Blau und Weiß).  
Informationen zu allen Facebook-Plugins finden Sie über den folgenden Link:  
<https://developers.facebook.com/docs/plugins/>

Facebook Inc. hält das europäische Datenschutzrecht ein und ist unter dem Privacy-Shield-Abkommen  
zertifiziert: <https://www.privacyshield.gov/participant?id=a2zt0000000GnywAAC&status=Active>

Das Plugin stellt eine direkte Verbindung zwischen Ihrem Browser und den Facebook-Servern her.  
Der Websitebetreiber hat keinerlei Einfluss auf die Natur und den Umfang der Daten,  
welche das Plugin an die Server der Facebook Inc. übermittelt.  
Informationen dazu finden Sie hier: <https://www.facebook.com/help/186325668085084>

Das Plugin informiert die Facebook Inc. darüber, dass Sie als Nutzer diese Website besucht haben.  
Es besteht hierbei die Möglichkeit, dass Ihre IP-Adresse gespeichert wird. Sind Sie während des Besuchs  
auf dieser Website in Ihrem Facebook-Konto eingeloggt, werden die genannten Informationen mit diesem verknüpft.  

Nutzen Sie die Funktionen des Plugins – etwa indem Sie einen Beitrag teilen oder „liken“ –,  
werden die entsprechenden Informationen ebenfalls an die Facebook Inc. übermittelt.  
Möchten Sie verhindern, dass die Facebook. Inc. diese Daten mit Ihrem Facebook-Konto verknüpft,  
loggen Sie sich bitte vor dem Besuch dieser Website bei Facebook aus und löschen Sie die gespeicherten Cookies.  
Über Ihr Facebook-Profil können Sie weitere Einstellungen zur Datenverarbeitung  
für Werbezwecke tätigen oder der Nutzung Ihrer Daten für Werbezwecke widersprechen.  
Zu den Einstellungen gelangen Sie hier:  

• Profileinstellungen bei Facebook: <https://www.facebook.com/ads/preferences/?entry_product=ad_settings_screen>  
• Cookie-Deaktivierungsseite der US-amerikanischen Website: <http://optout.aboutads.info/?c=2#!/>  
• Cookie-Deaktivierungsseite der europäischen Website: <http://optout.networkadvertising.org/?c=1#!/>  

Welche Daten, zu welchem Zweck und in welchem Umfang Facebook Daten erhebt,  
nutzt und verarbeitet und welche Rechte sowie Einstellungsmöglichkeiten Sie zum Schutz Ihrer Privatsphäre haben,  
können Sie in den Datenschutzrichtlinien von Facebook nachlesen.  
Diese finden Sie hier: <https://www.facebook.com/about/privacy/>  

## Nutzung von Plugins von YouTube ##

Unsere Website nutzt Plugins der von Google betriebenen Seite YouTube.  
Betreiber der Seiten ist die YouTube, LLC, 901 Cherry Ave., San Bruno, CA 94066, USA.  

Wenn Sie eine unserer mit einem YouTube-Plugin ausgestatteten Seiten besuchen,  
wird eine Verbindung zu den Servern von YouTube hergestellt.  
Dabei wird dem YouTube-Server mitgeteilt, welche unserer Seiten Sie besucht haben.  

Wenn Sie in Ihrem YouTube-Account eingeloggt sind, ermöglichen Sie YouTube,  
Ihr Surfverhalten direkt Ihrem persönlichen Profil zuzuordnen.  
Dies können Sie verhindern, indem Sie sich aus Ihrem YouTube-Account ausloggen,  
bevor sie unsere Website besuchen.  

Die Nutzung von YouTube erfolgt im Interesse einer ansprechenden Darstellung unserer Online-Angebote.  
Dies stellt ein berechtigtes Interesse im Sinne von Art. 6 Abs. 1 lit. f DSGVO dar.  

Weitere Informationen zum Umgang mit Nutzerdaten finden Sie in der Datenschutzerklärung von YouTube unter:  
<https://www.google.de/intl/de/policies/privacy>.  

## Google Web Fonts ##

Diese Seite nutzt zur einheitlichen Darstellung von Schriftarten so genannte Web Fonts,  
die von Google bereitgestellt werden. Beim Aufruf einer Seite lädt Ihr Browser die benötigten Web Fonts  
in ihren Browsercache, um Texte und Schriftarten korrekt anzuzeigen.  

Zu diesem Zweck muss der von Ihnen verwendete Browser Verbindung zu den Servern von Google aufnehmen.  
Hierdurch erlangt Google Kenntnis darüber, dass über Ihre IP-Adresse unsere Website aufgerufen wurde.  
Die Nutzung von Google Web Fonts erfolgt im Interesse einer einheitlichen und ansprechenden Darstellung  
unserer Online-Angebote. Dies stellt ein berechtigtes Interesse im Sinne von Art. 6 Abs. 1 lit. f DSGVO dar.  

Weitere Informationen zu Google Web Fonts finden Sie unter <https://developers.google.com/fonts/faq>  
und in der Datenschutzerklärung von Google: <https://www.google.com/policies/privacy/.>  

## Google Maps ##

Diese Seite nutzt über eine API den Kartendienst Google Maps.  
Anbieter ist die Google Inc., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA.  

Zur Nutzung der Funktionen von Google Maps ist es notwendig, Ihre IP Adresse zu speichern.  
Diese Informationen werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert.  
Der Anbieter dieser Seite hat keinen Einfluss auf diese Datenübertragung.  

Die Nutzung von Google Maps erfolgt im Interesse einer ansprechenden Darstellung unserer Online-Angebote und an  
einer leichten Auffindbarkeit der von uns auf der Website angegebenen Orte. Dies stellt ein berechtigtes Interesse  
im Sinne von Art. 6 Abs. 1 lit. f DSGVO dar.  

Mehr Informationen zum Umgang mit Nutzerdaten finden Sie in der Datenschutzerklärung von Google:  
<https://www.google.de/intl/de/policies/privacy/>.  

## Links auf Websites anderer Anbieter ##

Wir haben auf unserer Website Links auf Websites anderer Anbieter gesetzt.  
Wir sind für die Datenverarbeitung auf diesen Websites nicht verantwortlich.  
Wie die jeweiligen Anbieter den Datenschutz handhaben, entnehmen Sie bitte deren Datenschutzbestimmungen.  

## Rechte des Nutzers ##

Sie haben als Nutzer das Recht, auf Antrag eine kostenlose Auskunft darüber zu erhalten,  
welche personenbezogenen Daten über Sie gespeichert wurden. Sie haben außerdem das Recht auf  
Berichtigung falscher Daten und auf die Verarbeitungseinschränkung oder Löschung Ihrer personenbezogenen Daten.  
Falls zutreffend, können Sie auch Ihr Recht auf Datenportabilität geltend machen. Sollten Sie annehmen,  
dass Ihre Daten unrechtmäßig verarbeitet wurden, können Sie eine Beschwerde bei der zuständigen Aufsichtsbehörde einreichen.  

## Löschung von Daten ##

Sofern Ihr Wunsch nicht mit einer gesetzlichen Pflicht zur Aufbewahrung von Daten (z. B. Vorratsdatenspeicherung) kollidiert,  
haben Sie ein Anrecht auf Löschung Ihrer Daten. Von uns gespeicherte Daten werden, sollten sie für ihre Zweckbestimmung nicht  
mehr vonnöten sein und es keine gesetzlichen Aufbewahrungsfristen geben, gelöscht.  
Falls eine Löschung nicht durchgeführt werden kann, da die Daten für zulässige gesetzliche Zwecke erforderlich sind,  
erfolgt eine Einschränkung der Datenverarbeitung. In diesem Fall werden die Daten gesperrt und nicht für andere Zwecke verarbeitet.  

## Widerspruchsrecht ##

Nutzer dieser Webseite können von ihrem Widerspruchsrecht Gebrauch machen und der  
Verarbeitung ihrer personenbezogenen Daten zu jeder Zeit widersprechen.  

Wenn Sie eine Berichtigung, Sperrung, Löschung oder Auskunft über die zu Ihrer Person  
gespeicherten personenbezogenen Daten wünschen oder Fragen bzgl. der Erhebung,  
Verarbeitung oder Verwendung Ihrer personenbezogenen Daten haben oder erteilte  
Einwilligungen widerrufen möchten, wenden Sie sich bitte an folgende E-Mail-Adresse:  
sks.sahin@web.de

Hinweis: Je nachdem welche personenbezogenen Daten, Cookies, Plugins etc. Sie auf Ihrer  
Webseite verwenden, müssen Sie in Ihrer Datenschutzerklärung weitere Informationen aufführen.  

Dazu gehören beispielsweise Informationen über (Liste ist nicht vollständig):  

    • Die Weitergabe von Daten an Dritte und Drittanbieter
    • Einbindungen von Diensten und Inhalten Dritter (z. B. Google-Fonts oder YouTube-Videos)
    • Erbringung vertraglicher Leistungen
    • Kontaktaufnahme über das Kontaktformular
    • Verwendung von Session-Cookies
    • Einsatz von datenverarbeitenden Anti-Spam-Plugins
    • Nutzung des Google Remarketing-Tags
    • Verwendung von Google+ oder Twitter-Schaltflächen
