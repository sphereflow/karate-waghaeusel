
## Anfänger ##

Kinder ab 11 Jahren lernen zusätzlich Programm:  

- Ihren ersten Karate Stand statisch und in der Bewegung.  
- Einzelne Grundtechniken aus dem Stand und in der Bewegung.  
- Ihre erste Kata (komplexerer Bewegungsablauf).  
Sie werden somit auf ihre erste Gürtelprüfung vorbereitet.  

Bei Interesse schicken sie uns eine e-Mail an: info@karate-waghäusel.de

---

## Fortgeschrittene ##

Fortgeschrittenen bieten wir die Möglichkeit komplexere Abläufe und Technikkombinationen  
zu erlernen und ihre Grundlagen auf ein höheres Niveau zu bringen.  
Auschlaggebend sind hier erhöhtes Leistungsvermögen, Koordination, Präzission, Disziplin und Kime.  
