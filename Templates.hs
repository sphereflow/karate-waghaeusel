{-# LANGUAGE ExtendedDefaultRules #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}

module Templates
  ( defTemplate,
    indexBody,
    testBody,
    imageBody,
    anfahrtBody,
    contactBody,
    defaultMarkdownBody,
  )
where

import Control.Monad (forM_)
import Data.Monoid
import Data.Text (Text)
import qualified Data.Text as TS
import Data.Text.Lazy (unpack)
import GHC.Exts (IsString (..))
import Lucid.Base
import Lucid.Bootstrap
import Lucid.Html5

defTemplate :: Html () -> String
defTemplate = unpack . renderText . defBlaze

-- basic template

defBlaze :: Html () -> Html ()
defBlaze actualBody = do
  doctype_
  html_ $ do
    defHead
    -- testBody
    defBody actualBody

domain = "https://www.karate-waghäusel.de"

description :: Text
description = "Website des Karate Leistungskaders Waghäusel e. V."

navItems :: [(String, String)]
navItems =
  [ ("Home", "index.html"),
    ("Kinder", "kinder.html"),
    ("Erwachsene", "erwachsene.html"),
    ("Training", "training.html"),
    ("Über uns", "personen.html"),
    ("Aktuelles", "aktuelles.html"),
    ("Wettkampf", "wettkampf.html"),
    -- , ("Bilder", "bilder.html")
    ("Anfahrt", "anfahrt.html"),
    ("Downloads", "downloads.html")
  ]

footerLinks :: [(Text, Text)]
footerLinks =
  [ ("Besuche uns auf Facebook", facebookLink),
    ("Kontakt / Impressum", "impressum.html"),
    ("Datenschutzerklärung", "datenschutzerklaerung.html")
  ]

facebookLink :: Text
facebookLink = "https://de-de.facebook.com/Karate-Wagh%C3%A4usel-344013378987501/"

defHead :: Html ()
defHead =
  head_ $ do
    meta_ [charset_ "utf-8"]
    meta_ [httpEquiv_ "X-UA-Compatible", content_ "IE=edge"]
    meta_ [name_ "viewport", content_ "width=device-width, initial-scale=1"]
    meta_ [name_ "description", content_ description]
    link_ [rel_ "icon", href_ "icon.ico"]
    title_ "$title$"
    link_ [rel_ "stylesheet", href_ "./css/bootstrap.min.css"]
    link_
      [ href_ "https://fonts.googleapis.com/css?family=Quicksand",
        rel_ "stylesheet"
      ]
    link_ [rel_ "stylesheet", href_ "./css/default.css"]
    meta_ [property_ "ol:url", content_ domain]
    meta_ [property_ "ol:type", content_ "website"]
    meta_ [property_ "ol:title", content_ "Karate Waghäusel"]
    meta_ [property_ "ol:description", content_ description]
    meta_ [property_ "ol:image", content_ domain]

defBody :: Html () -> Html ()
defBody actualBody =
  body_ $ do
    navBar
    div_ [id_ "main_div", class_ "container"] $ do
      actualBody
      defFooter
    script_ [src_ "https://code.jquery.com/jquery-3.3.1.slim.min.js", integrity_ "sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo", crossorigin_ "anonymous"] ""
    script_ [src_ "./js/bootstrap.min.js"] ""
    script_ [src_ "./js/menuTable.js"] ""

defFooter :: Html ()
defFooter = footer_ [class_ "footer"] $
  div_ [class_ "container"] $
    div_ [class_ "row"] $
      forM_ footerLinks $
        \(description, link) -> div_ [class_ "col-sm"] $ a_ [href_ link] ((fromString . TS.unpack) description)

-- end of basic template

-- different bodys for different pages

testBody :: Html ()
testBody =
  body_ $
    h1_ "In is the only way out!"

indexBody :: Html ()
indexBody =
  div_ [class_ "container", id_ "index_container"] $ do
    -- listing "Karate Waghäusel" []
    img_ [class_ "img-fluid", src_ "/images/klwlogo.jpg"]
    br_ []
    div_ [class_ "container"] $
      div_ [class_ "row", style_ "margin-top:100px"] $
        forM_ logos $ \logo -> img_ [class_ "img-thumbnail", src_ logo]
    fbLikeShare

logos :: [Text]
logos = fromString <$> ["images/kvbwlogo.png", "/images/dkvlogo.png", "images/lsvlogo.png", "images/dosblogo.png", "images/wkflogo.png"]

imageBody :: [String] -> Html ()
imageBody images = div_ [class_ "container", id_ "image_container"] $ do
  makeCarousel (Just images) imageItem
  fbLikeShare

imageItem :: Maybe Text -> Bool -> Html ()
imageItem Nothing active
  | active = img_ [class_ "item container img-fluid active", src_ "$imgUrl$"]
  | otherwise = img_ [class_ "item container img-fluid", src_ "$imgUrl$"]
imageItem (Just url) active
  | active = img_ [class_ "item img-fluid container active", src_ url]
  | otherwise = img_ [class_ "item img-fluid container", src_ url]

makeCarousel :: Maybe [String] -> (Maybe Text -> Bool -> Html ()) -> Html ()
makeCarousel files item = div_ [id_ "carouselid", class_ "carousel slide", dataRide_ "carousel", dataInterval_ "false"] $ do
  case files of
    Nothing -> do
      ol_ [class_ "carousel-indicators"] $ do
        li_ [dataTarget_ "#carouselid", dataSlideTo_ "0", class_ "active"] ""
        "$for(numbers)$"
        li_ [dataTarget_ "#carouselid", dataSlideTo_ "$number$"] ""
        "$endfor$"
      div_ [class_ "carousel-inner", role_ "listbox"] $ do
        -- hack alert
        "$for(firstItem)$"
        item Nothing True
        "$endfor$"
        "$for(items)$"
        item Nothing False
        "$endfor$"
    (Just fileList) -> do
      ol_ [class_ "carousel-indicators"] $ do
        li_ [dataTarget_ "#carouselid", dataSlideTo_ "0", class_ "active"] ""
        forM_
          [1 .. (length fileList - 1)]
          (\num -> li_ [dataTarget_ "#carouselid", dataSlideTo_ ((fromString . show) num)] "")
      div_ [class_ "carousel-inner", role_ "listbox"] $ do
        item ((Just . fromString . head) fileList) True
        forM_
          (tail fileList)
          (\fname -> item ((Just . fromString) fname) False)
  a_ [class_ "left carousel-control", href_ "#carouselid", role_ "button", dataSlide_ "prev"] $ do
    span_ [class_ "glyphicon glyphicon-chevron-left", ariaHidden_ "true"] ""
    span_ [class_ "sr-only"] "Zurück"
  a_ [class_ "right carousel-control", href_ "#carouselid", role_ "button", dataSlide_ "next"] $ do
    span_ [class_ "glyphicon glyphicon-chevron-right", ariaHidden_ "true"] ""
    span_ [class_ "sr-only"] "Vor"

menuTable :: Int -> Html ()
menuTable i =
  table_ [class_ "table table-bordered"] $
    thead_ $ do
      tr_ $ do
        th_ "Menü"
        th_ "Nr."
        th_ "item"
        th_ "Preis"
      tbody_ [] "$table$" -- find out how to do iteration with pandoc
      -- find a suitable pandoc format for the menu table

fbLikeShare :: Html ()
fbLikeShare = do
  div_ [id_ "fb-root"] ""
  script_ [src_ "./js/fbscript.js"] ""
  div_
    [ class_ "fb-like",
      id_ "fb_like_share",
      dataHref_ domain,
      dataLayout_ "standard",
      dataAction_ "like",
      dataShowFaces_ "true",
      dataShare_ "true"
    ]
    ""

-- custom attributes

dataHref_ :: Text -> Attribute
dataHref_ = makeAttribute "data-href"

dataTarget_ :: Text -> Attribute
dataTarget_ = makeAttribute "data-target"

dataToggle_ :: Text -> Attribute
dataToggle_ = makeAttribute "data-toggle"

dataSlideTo_ :: Text -> Attribute
dataSlideTo_ = makeAttribute "data-slide-to"

ariaHidden_ :: Text -> Attribute
ariaHidden_ = makeAttribute "aria-hidden"

dataSlide_ :: Text -> Attribute
dataSlide_ = makeAttribute "data-slide"

dataInterval_ :: Text -> Attribute
dataInterval_ = makeAttribute "data-interval"

dataRide_ :: Text -> Attribute
dataRide_ = makeAttribute "data-ride"

dataLayout_ :: Text -> Attribute
dataLayout_ = makeAttribute "data-layout"

dataAction_ :: Text -> Attribute
dataAction_ = makeAttribute "data-action"

dataShowFaces_ :: Text -> Attribute
dataShowFaces_ = makeAttribute "data-show-faces"

property_ :: Text -> Attribute
property_ = makeAttribute "property"

dataShare_ :: Text -> Attribute
dataShare_ = makeAttribute "data-share"

frameborder_ :: Text -> Attribute
frameborder_ = makeAttribute "frameborder"

ariaControls_ :: Text -> Attribute
ariaControls_ = makeAttribute "aria-controls"

ariaExpanded_ :: Text -> Attribute
ariaExpanded_ = makeAttribute "aria-expanded"

ariaLabel_ :: Text -> Attribute
ariaLabel_ = makeAttribute "aria-label"

-- navigation bar

navBar :: Html ()
navBar =
  nav_ [class_ "navbar navbar-expand-lg navbar-light"] $ do
    button_
      [ class_ "navbar-toggler",
        type_ "button",
        dataToggle_ "collapse",
        dataTarget_ "#navbarSupportedContent",
        ariaControls_ "navbarSupportedContent",
        ariaExpanded_ "false",
        ariaLabel_ "Toggle navigation"
      ]
      $ span_ [class_ "navbar-toggler-icon"] ""
    div_ [class_ "collapse navbar-collapse", id_ "navbarSupportedContent"] $
      div_ [class_ "container", id_ "nav_container"] $
        navLinks navItems

navBrand :: Html ()
navBrand =
  div_ [class_ "navbar-header"] $
    a_ [class_ "navbar-brand", href_ "index.html"] $ h2_ [class_ "home-text"] "Home"

navLinks :: [(String, String)] -> Html ()
navLinks navItems =
  ul_ [class_ "navbar-nav mr-auto mt-2 mt-lg-0 justify-content-center"] $
    forM_ navItems $ \(name, link) ->
      li_ [class_ "nav-item"] $
        a_ [class_ "nav-link btn_underline mr-2", href_ (fromString link)] $ h3_ [class_ "nav-item-text"] (fromString name)

-- composable elements

anfahrtBody :: Html ()
anfahrtBody = div_ [class_ "container", id_ "frame_container"] $ do
  anfahrtAddresse
  googleMapsIFrame
  fbLikeShare

anfahrtAddresse :: Html ()
anfahrtAddresse =
  div_ [class_ "jumbotron listing"] $ do
    h2_ $ do
      span_ [class_ "glyphicon glyphicon-map-marker"] ""
      "Anfahrt"
    h3_ "Fitness Plaza"
    h3_ "Nördliche Waldstraße 11a"
    h3_ "68753 Waghäusel-Kirrlach"

googleMapsIFrame :: Html ()
googleMapsIFrame =
  iframe_
    [ src_
        "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3097.2668161531683!2d8.535458910815885!3d49.24833669978381!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4797bafb30ce2625%3A0xc61ce05c15079104!2sKarate+Wagh%C3%A4usel!5e0!3m2!1sde!2sde!4v1530292073023",
      width_ "100%",
      height_ "450",
      frameborder_ "0",
      style_ "border:0"
    ]
    ""

contactBody :: Html ()
contactBody = div_ [class_ "container", id_ "contact_container"] $ do
  anfahrtAddresse
  listing "Telefon: 0176/27271901" []
  listing "E-mail: sks.sahin@web.de" []
  listing "Impressum :" ["Tahsin Sahin", "TODO : str hn", "68753 Waghäusel-Kirlach"]
  fbLikeShare

defaultMarkdownBody :: Html ()
defaultMarkdownBody = div_ [class_ "container"] "$body$"

listing :: Term a (Html ()) => a -> [a] -> Html ()
listing header list =
  div_ [class_ "jumbotron listing"] $ do
    h2_ header
    forM_ list h3_
