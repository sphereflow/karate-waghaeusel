# Trainingsplan #

| Montag                         | Dienstag                              | Mittwoch                              | Donnerstag                            | Freitag | Samstag                                      | Sonntag                                                                     |
|:------------------------------:|:-------------------------------------:|:-------------------------------------:|:-------------------------------------:|:-------:|:--------------------------------------------:|:---:|
|                                |                                       |                                       |                                       |         | *18:30 - 19:30* <br> Kindertraining          |     |
|                                |                                       |                                       |                                       |         |                                              |     |
|                                | *18:30 - 19:30* <br> Kindertraining   |                                       |                                       |         |                                              |     |
|                                | *19:30 - 20:30* <br> Erwachsene + Ü14 |                                       |                                       |         |                                              |     |

# Allgemeines zum Training #

Folgende Richtlinien sind beim Training zu beachten:  

- Wenn ihr den Trainingsraum betretet könnt ihr euch verbeugen.  
  Stellt eure Schuhe und andere Sachen auf die Metallablage oder auf die Schuhablage ab.  
  
- Wenn gerade ein Training läuft, dann verhaltet euch leise (nicht reden).  
  Setzt euch auf die blaue Matte in Seiza ab.  
  
- Wenn ihr zu spät zum Training kommt setzt euch bitte gut sichtbar an den Rand der Trainingsfläche in Seiza ab  
  und wartet bis der Trainer euch auffordert mitzumachen.  

- Trainiert wird nur barfuß, in Turnschläppchen, oder in rutschfesten Socken.  

- Bitte legt vor dem Training Uhren und Schmuck ab.  
  Es besteht Verletzungsgefahr.  
  
- Ärmel und Hosenbeine werden nach innen gekrempelt.  

- Es ist im Allgemeinen ein Karateanzug zu tragen. Ein weißes T-Shirt kann unter der Karatejacke getragen werden.  

- Bei hohen Temperaturen ( ab 30 Grad ) könnt ihr ein weißes T-Shirt anstatt der Karatejacke anziehen.  

- Zum Probetraining könnt ihr 2 Wochen lang in zum Sport geeigneten Sachen kommen.  
