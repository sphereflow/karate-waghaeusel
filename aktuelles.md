
- # Aktuelle Lage: Trainingsbetrieb während der Corona Krise #
### Aufgrund der aktuellen Corona-Verordnung und Wetterlage hat der Vereinsvorstand beschlossen, dass wir unser Training auf dem Schulhof der Goethe-Schule abhalten. 

---

- # Prüfung: #
   Die letzte Prüfung fand am Samstag den 14.12.2019 statt.  
   Der nächste Prüfungstermin ist noch nicht bekannt.  

---

- # Ferien #
  Änderungen am Trainingsbetrieb für die kommenden Ferien sind noch nicht bekannt.  

---

- # Wettkampf #
   Das letzte Turnier war die Landesmeisterschaft Kinder/Schüler am 06.10.2018.  
   Shirin und Jasin haben beide den 5.Platz gemacht.  
   Termine für kommende Turniere könnt ihr bei eurem Wettkampftrainer nachfragen.

---

- # Verein #
  Die letzte Mitgliederversammlung fand am Samstag den 24.09.2022 statt.  
   
